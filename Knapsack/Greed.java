package selcouth.tijana.sise;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class Greed {

	private static ArrayList<Item> allItems = new ArrayList<Item>();
	private static HashMap<String, String> solutions = new HashMap<String, String>();
	
	public static void solve(ArrayList<Item> items, ArrayList<Knapsack> knapsacks) {
		
		allItems.addAll(items);
		Collections.sort(allItems);
		Knapsack[] KParray = new Knapsack[knapsacks.size()];
		
		for(int i = 0; i < knapsacks.size(); i++) {
			KParray[i] = knapsacks.get(i);
		}
		
		premute(KParray);
		
		String bestSolution = "0-0-0";
		int bestValue = 0;
		for(Map.Entry<String, String> solution : solutions.entrySet()) {
			int value = Integer.valueOf(solution.getValue().split("/")[0]);
			if(value > bestValue) {
				bestValue = value;
				bestSolution = solution.getKey();
			}
		}
		
		File solutionFile = new File(bestSolution);
		System.out.println("Premutation: " + bestSolution);
		try(Scanner sc = new Scanner(solutionFile)) {
			while(sc.hasNext()) {
				System.out.println(sc.nextLine());
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		
	}
	
	private static void summonSimpleGreed(Knapsack[] kpArray) {
		LinkedList<Knapsack> kpList = new LinkedList<Knapsack>();
		String tmp = "";
		for(int i = 0; i < kpArray.length; i++) {
			kpList.addFirst(kpArray[i]);
			tmp += kpArray[i].getNumber();
			if(i != kpArray.length-1)
				tmp += "-";
		}
		
		File file = new File(tmp);
		PrintStream console = System.out;
		
		
		long start = System.nanoTime();
		String res = SimpleGreed.solve(allItems, kpArray, file);
		long end = System.nanoTime();
		double duration = (double) (end - start) / 1000000;
		
		solutions.put(tmp, res+"/"+duration);
		System.out.println("Time: " + duration + "ms");
		System.out.println("\n\n####################################################\n\n");
		System.setOut(console);
		
	}
	
	private static void premute(Knapsack[] array) {
		helper(array, 0);
	}
	
	private static void helper(Knapsack[] array, int pos) {
		
		if(pos >= array.length -1) {
			for(int i = 0; i < array.length - 1; i++) {
			//	System.out.print(array[i] + " ") || NO TOUCH OR SHIT NO WORK
			}
			if(array.length > 0)
			summonSimpleGreed(array);
			return;
		}
		
		for(int i = pos; i < array.length; i++) {
			Knapsack t = array[pos];
			array[pos] = array[i];
			array[i] = t;
			
			helper(array, pos+1);
			
			t = array[pos];
			array[pos] = array[i];
			array[i] = t;
			
		}
	}
	
}
