package selcouth.tijana.sise;

import java.util.ArrayList;

public class Knapsack {
	
	private int maxWeight;
	private int currWeight;
	private int number;
	private ArrayList<Integer> coloursAllowed;
	private int currValue;
	private ArrayList<Item> items;
	private double valueByWeight;
	
	public Knapsack(int maxW, int number, ArrayList<Integer> coloursA) {
		this.maxWeight = maxW;
		this.currWeight = 0;
		this.number = number;
		this.coloursAllowed = coloursA;
		this.currValue = 0;
		this.items = new ArrayList<Item>();
		this.valueByWeight = 0;
	}
	
	
	public int getMaxWeight() {
		return maxWeight;
	}

	public int getCurrWeight() {
		return currWeight;
	}

	public int getNumber() {
		return number;
	}

	public ArrayList<Integer> getColoursAllowed() {
		return coloursAllowed;
	}

	public void addWeight(int weight) {
		this.currWeight += weight;
	}
	
	public boolean allowsColour(Integer colour) {
		return this.coloursAllowed.contains(colour);
	}
	
	public boolean allowsColours(ArrayList<Integer> itemColours) {
		for(Integer colour : itemColours) {
			if(this.coloursAllowed.contains(colour))
				return true;
		}
		return false;
	}

	public int getCurrValue() {
		return currValue;
	}

	public void addValue(int value) {
		this.currValue += value;
	}
	
	public void addItem(Item newItem) {
		this.items.add(newItem);
		this.currValue += newItem.getValue();
		this.currWeight += newItem.getWeight();
		this.valueByWeight = (double) this.currValue / (double) this.currWeight;
	}
	
	public double getValueByWeight() {
		return this.valueByWeight;
	}
	
	public boolean hasItem(Item item) {
		return this.items.contains(item);
	}
	
	public ArrayList<Item> getItems() {
		return this.items;
	}
	
	public boolean canFit(int weight) {
		if((this.currWeight + weight) > this.maxWeight)
			return false;
		else
			return true;
	}
	
	public void empty() {
		this.items.clear();
		this.currValue = 0;
		this.currWeight = 0;
		this.valueByWeight = 0;
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null || this.getClass() != obj.getClass())
			return false;
		
		Knapsack kp = (Knapsack)obj;
		
		return (this.coloursAllowed.equals(kp.getColoursAllowed()) && this.maxWeight == kp.getMaxWeight());
	}
	
	public void print() {
		System.out.println("Knapsack Number " + String.valueOf(this.number));
		System.out.println("Weight (curr/max) : " + this.currWeight + "/" + this.maxWeight);
		System.out.println("Value: " + this.currValue + " Value/Weight: " + this.valueByWeight);
		System.out.print("Colours Allowed: ");
		for(Integer colour : this.coloursAllowed) {
			System.out.print(colour + " ");
		}
		System.out.print("\n");
		System.out.println("CONTENT:");
		for(Item item : this.items) {
			System.out.println(item);
		}
		System.out.println("=====================\n");
	}
	
	@Override
	public String toString() {
		return String.valueOf(this.number);
	}
}
