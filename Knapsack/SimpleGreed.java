package selcouth.tijana.sise;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;

public class SimpleGreed {

	public static String solve(ArrayList<Item> items, Knapsack[] knapsacks, File file) {
		
		Collections.sort(items);
		
		for(int i = 0; i < knapsacks.length; i++) {
			for(Item item : items) {
				if(knapsacks[i].allowsColours(item.getColours())
						&& knapsacks[i].canFit(item.getWeight()))
					knapsacks[i].addItem(item);

				if(knapsacks[i].getCurrWeight() == knapsacks[i].getMaxWeight())
					break;
			}
			items.removeAll(knapsacks[i].getItems());
		}
		
		int totalWeight = 0;
		int totalValue = 0;
		
		PrintStream stream;
		try {
			stream = new PrintStream(file);
			System.setOut(stream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		for(Knapsack kp : knapsacks) {
			kp.print();
			totalWeight += kp.getCurrWeight();
			totalValue += kp.getCurrValue();
			items.addAll(kp.getItems());
			kp.empty();
		}
		
		System.out.println("TOTAL\nValue/Weight : " + totalValue + "/" + 
				totalWeight);
		
		return totalValue + "/" + totalWeight;
	}
}
