package selcouth.tijana.sise;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	
		String path2items = args[0];
		String path2knapsacks = args[1];
		
		File itemsFile = new File(path2items);
		File knapsacksFile = new File(path2knapsacks);
		
		ArrayList<Item> allItems = new ArrayList<Item>();
		ArrayList<Knapsack> allKnapsacks = new ArrayList<Knapsack>();
		
		try(Scanner sc = new Scanner(itemsFile)) {
			String[] tmp;
			while(sc.hasNext()) {
				tmp = sc.nextLine().split("_");
				Item tmpItem = new Item(Integer.valueOf(tmp[0]), Integer.valueOf(tmp[1]));
				String[] tmpCol = tmp[2].split(",");
				for(int i = 0; i < tmpCol.length; i++) {
					tmpItem.addColour(Integer.valueOf(tmpCol[i]));
				}
				if(!allItems.contains(tmpItem))
					allItems.add(tmpItem);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try(Scanner sc = new Scanner(knapsacksFile)) {
			String[] tmp;
			String[] tmpColours;
			while(sc.hasNext()) {
				ArrayList<Integer> tmpColoursA = new ArrayList<Integer>();
				tmp = sc.nextLine().split("_");
				tmpColours = tmp[1].split(",");
				for(int i = 0; i < tmpColours.length; i++) {
						tmpColoursA.add(Integer.valueOf(tmpColours[i]));
				}
				Knapsack tmpKP = new Knapsack(Integer.valueOf(tmp[0]), allKnapsacks.size()+1, tmpColoursA);
				allKnapsacks.add(tmpKP);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Greed.solve(allItems, allKnapsacks);
	}

}
