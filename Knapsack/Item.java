package selcouth.tijana.sise;

import java.util.ArrayList;

public class Item implements Comparable<Item>{
	
	private int value;
	private int weight;
	private double valByWeight;
	private ArrayList<Integer> colours;
	
	public Item(int value, int weight) {
		this.value = value;
		this.colours = new ArrayList<Integer>();
		this.weight = weight;
		this.valByWeight = (double) value / (double) weight;
	}

	public int getValue() {
		return value;
	}

	public ArrayList<Integer> getColours() {
		return colours;
	}

	public void addColour(int colour) {
		this.colours.add(Integer.valueOf(colour));
	}

	public int getWeight() {
		return weight;
	}

	public double getValueByWeight() {
		return this.valByWeight;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null || this.getClass() != obj.getClass())
			return false;
		
		Item it = (Item)obj;
		
		return this.colours.equals(it.getColours()) && this.value == it.getValue() && this.weight == it.getWeight();
	}

	@Override
	public int compareTo(Item arg0) {
		if(this.valByWeight > arg0.getValueByWeight())
			return -1;
		else if(arg0.getValueByWeight() > this.valByWeight)
			return 1;
		else if(arg0.getWeight() < this.weight)
			return 1;
		else
			return -1;
	}
	
	@Override
	public String toString() {
		String colours = "C:";
		for(Integer colour : this.colours) {
			colours += " " + colour;
		}
		String toReturn = colours + " V: " + this.value + " W: " + this.weight + " V/W: " + this.valByWeight;
		return toReturn;
	}
	
	
}