package fax.projekt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class Robot {
	private String goal;
	private ArrayList<String> nghbrs = new ArrayList<String>();
	private Grid grid;
	private String currLoc;
	private LinkedList<String> pathTaken = new LinkedList<String>();
	private float I;
	private String start;
	
	public Robot(String start, String g, Grid gr, float i) {
		this.goal = g;
		this.grid = gr;
		this.grid.getNode(this.goal).setGoal();
		this.currLoc = start;
		this.start = start;
		this.grid.getNode(this.currLoc).setActive(true);
		pathTaken.addLast(currLoc);
		this.refreshNghb();
		this.I = i;
	}
	
	public LinkedList<String> findGoal() {
		while(!currLoc.equals(goal)) {
			grid.DCcurrent(I, currLoc);
			currLoc = grid.LeastResist(currLoc, pathTaken);
			pathTaken.addLast(currLoc);
			this.refreshNghb();
		}
		return pathTaken;
	}
	
	private void refreshNghb() {
		nghbrs.clear();
		int rw = Integer.valueOf(currLoc.split(",")[0]);
		int cl = Integer.valueOf(currLoc.split(",")[1]);
		ArrayList<String> potNb = new ArrayList<String>();
		potNb.add((rw+1) + "," + cl);
		potNb.add((rw-1) + "," + cl);
		potNb.add(rw + "," + (cl+1));
		potNb.add(rw + "," + (cl-1));
		for(String nb : potNb)
			if(!grid.isWall(currLoc + "-" + nb) && !pathTaken.contains(nb))
				nghbrs.add(nb);
	}
	
	public HashMap<Node, Node> Astar() {
		ArrayList<Node> allNodes = grid.getAllNodes();
		LinkedList<Node> open = new LinkedList<Node>();
		LinkedList<Node> closed = new LinkedList<Node>();
		HashMap<Node, Node> pathTaken = new HashMap<Node, Node>();
		HashMap<String, Integer> distances = new HashMap<String, Integer>();
		for(Node n : allNodes) {
			distances.put(n.getLocation(), Integer.MAX_VALUE);
		}
		distances.replace(start, 0);
		open.add(grid.getNode(start));
		while(!open.isEmpty()) {
			Node n = open.removeFirst();
			if(!closed.contains(n)) {
				closed.add(n);
				if(n.getLocation().equals(goal))
					return pathTaken;
				ArrayList<Node> goodNghb = grid.getGoodNghb(n);
				for(Node nghb : goodNghb) {
					if(!closed.contains(nghb)) {
						int distance = n.getDistTo(start) + 1;
						if(distance < distances.get(nghb.getLocation())) {
							distances.put(nghb.getLocation(), distance);
							pathTaken.put(nghb, n);
							open.add(n);
						}
					}
				}
			}
		}
		return null;
	}
}
