package fax.projekt;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;
import javax.swing.*;

public class Main {
	
	private static void printResults(LinkedList<String> path) {
		for(String location : path) {
			if(path.indexOf(location) == path.size()-1)
				System.out.println(location);
			else
				System.out.print(location + " -> ");				
		}
	}

	public static void main(String[] args) {
		int col;
		int row;
		int defR;
		Grid grid;
		String start;
		String goal;
		float amp;
		ArrayList<String> walls = new ArrayList<String>();
		Robot robot;
		LinkedList<String> path = new LinkedList<String>();
		int minCol = 0;
		int minRow = 0;
		
		if(args.length > 0) {
			String wallFilePath = args[0];
			File wallFile = new File(wallFilePath);
			try(Scanner sc = new Scanner(wallFile)) {
				String[] tmpA, tmpB, tmp;
				while(sc.hasNext()) {
					tmp = sc.nextLine().split("-");
					tmpA = tmp[0].split(",");
					tmpB = tmp[1].split(",");
					if(Integer.valueOf(tmpA[0]) > minRow)
						minRow = Integer.valueOf(tmpA[0]);
					if(Integer.valueOf(tmpB[0]) > minRow)
						minRow = Integer.valueOf(tmpB[0]);
					if(Integer.valueOf(tmpA[1]) > minCol)
						minCol = Integer.valueOf(tmpA[1]);
					if(Integer.valueOf(tmpB[1]) > minCol)
						minCol = Integer.valueOf(tmpB[1]);
					walls.add(sc.nextLine());
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			walls.add("1,1-2,1");
			walls.add("2,3-3,3");
			walls.add("2,2-3,2");
			walls.add("4,1-4,2");
			walls.add("1,2-1,3");
		}
		
		try(Scanner sc = new Scanner(System.in)) {
			do {
				System.out.println("Number of rows: ");
				int tmpRow = sc.nextInt();
				if(tmpRow < minRow) {
					System.out.println("Incompatible with walls! Minimum number of rows: " + minRow);
				} else {
					row = tmpRow;
					break;
				}
			} while (true);
			
			do {
				System.out.println("Number of columns: ");
				int tmpCol = sc.nextInt();
				if(tmpCol < minCol) {
					System.out.println("Incompatible with walls! Minimum number of columns: " + minCol);
				} else {
					col = tmpCol;
					break;
				}
			} while (true);
			
			System.out.println("Resistance of connections: ");
			defR = sc.nextInt();
			System.out.println("Starting position (row): ");
			int tmp1 = sc.nextInt();
			System.out.println("Starting position (column): ");
			int tmp2 = sc.nextInt();
			start = tmp1 + "," + tmp2;
			System.out.println("Goal (row): ");
			tmp1 = sc.nextInt();
			System.out.println("Goal (column): ");
			tmp2 = sc.nextInt();
			goal = tmp1 + "," + tmp2;
			grid = new Grid(row, col, defR, walls, goal);
			System.out.println("Electric current: ");
			amp = sc.nextFloat();
			robot = new Robot(start, goal, grid, amp);
			System.out.println("Press enter to begin");
			sc.nextLine();
		}
		
		for(Node n : grid.getAllNodes()) {
			n.setHeuristic(goal);
		}
		
		path.addAll(robot.findGoal());
	//	if(robot.Astar() != null) {
	//		System.out.println("ASTAR SUCCESS");
	//	}
		printResults(path);
		Mapper mapper = new Mapper(col, row, start, goal, walls, path);
	}

}
