package fax.projekt;

import java.util.ArrayList;
import java.util.LinkedList;

import Jama.Matrix;

public class Grid {
	private int columns;
	private int rows;
	private double[][] potential;
	private float defG;
	ArrayList<String> walls = new ArrayList<String>();
	private float amper;
	private double[][] conductanceGrid;
	ArrayList<Node> nodes = new ArrayList<Node>();
	private String goal;
	Matrix conductance;
	private int maxIndex;
	private double[] truepotential;
	int[] subIndices;
	
	public Grid (int r, int c, float R, ArrayList<String> w, String g) {
		this.columns = c;
		this.rows = r;
		this.goal = g;
		this.potential = new double[r][c];
		this.defG = 1/R;
		this.walls.addAll(w);
		this.subIndices = new int[rows*columns-1];
		this.conductanceGrid = new double[r*c][r*c];
		int index = 1;
		for(int i = 1; i <= this.rows; i++) {
			for(int j = 1; j <= this.columns; j++) {
				nodes.add(new Node(i, j, index));
				index++;
			}
		}
		this.truepotential = new double[r*c];
		this.maxIndex = index-1;
		getNode(this.goal).isGoal();
		setNeighbourhood();
		generateConductanceGrid(getNode(this.goal).getIndex());
	}
	
	public void setNeighbourhood() {
		String location = null;
		for(Node node : nodes) {
			location = node.getLocation();
			if(node.getRow() - 1 > 0) {
				Node nghbtmp = getNodeNum(node.getRow()-1, node.getColumn());
				if(!isWall(location + "-" + nghbtmp.getLocation())) {
					node.addNghb(nghbtmp);
				}
			}
			if(node.getRow() + 1 <= rows) {
				Node nghbtmp = getNodeNum(node.getRow()+1, node.getColumn());
				if(!isWall(location + "-" + nghbtmp.getLocation())) {
					node.addNghb(nghbtmp);
				}
			}
			if(node.getColumn() - 1 > 0) {
				Node nghbtmp = getNodeNum(node.getRow(), node.getColumn()-1);
				if(!isWall(location + "-" + nghbtmp.getLocation())) {
					node.addNghb(nghbtmp);
				}
			}
			if(node.getColumn() + 1 <= columns) {
				Node nghbtmp = getNodeNum(node.getRow(), node.getColumn()+1);
				if(!isWall(location + "-" + nghbtmp.getLocation())) {
					node.addNghb(nghbtmp);
				}
			}
		}
	}
	
	public ArrayList<Node> getAllNodes() {
		return this.nodes;
	}
	
	public ArrayList<Node> getGoodNghb(Node node) {
		ArrayList<Node> goodBoys = new ArrayList<Node>();
		for(Node nghb : node.getNghb()) {
			if(!isWall(node.getLocation() + "-" + nghb.getLocation()))
				goodBoys.add(nghb);
		}
		return goodBoys;
	}
 	
	public Node getNode(String name) {
		for(Node nod : nodes) {
			if(nod.getLocation().equals(name))
				return nod;
		}
		return null;
	}
	
	public Node getNodeNum(int row, int column) {
		for(Node node : nodes) {
			if(node.getRow() == row && node.getColumn() == column)
				return node;
		}
		return null;
	}
	
	public Node getNodeIndex(int index) {
		for(Node node : nodes) {
			if(node.getIndex() == index)
				return node;
		}
		return null;
	}
	
	public void generateConductanceGrid(int goalIndex) {
		for(int i = 1; i <= rows*columns; i++) {
			Node curr = getNodeIndex(i);
			for(int j = 1; j <= columns*rows; j++) {
				Node next = getNodeIndex(j);
				if(i == j) {
					//Diagonal values are equal to the number of neighbours
					conductanceGrid[i-1][j-1] = curr.getNghb().size();
				} else if(curr.getNghb().contains(next)) {
					//Neighbour relation values are equal to -I
					conductanceGrid[i-1][j-1] = -1;
				} else {
					//Unconected node relation values are 0
					conductanceGrid[i-1][j-1] = 0;
				}
			}
		}
		int j = 1;
		for(int i = 0; i < rows*columns-1; i++) {
			if(j != goalIndex) {
				subIndices[i] = j-1;
			} else {
				i--;
			}
			j++;
		}
		conductance = new Matrix(conductanceGrid);
		//Remove i*j columns and rows, and invert it
		conductance = conductance.getMatrix(subIndices, subIndices);
		conductance = conductance.inverse();
	}
	
	//Calculate potential of all nodes in the grid
	public void DCcurrent(float amp, String location) {
		this.amper = amp;
		Matrix current = new Matrix(rows * columns, 1);
		//Find the electrical input node
		int locationindex = (Integer.valueOf(location.split(",")[0]) - 1) * columns + Integer.valueOf(location.split(",")[1]);
		current.set(locationindex-1, 0, amp);
		current = current.getMatrix(subIndices, 0, 0);
		//Calculate the potential vector
		Matrix mPotential = conductance.times(current);
		potential = mPotential.getArrayCopy();
		
		//Copy results, ignoring j*i columns and rows
		for(int i = 0, j = 0; i < rows * columns - 1; i++, j++) {
			if(j+i == getNode(goal).getIndex()) {
				truepotential[j] = 0;
				i--;
			} else {
				truepotential[j] = potential[i][0];
			}
		}
		
	}
	
	public String LeastResist(String location, LinkedList<String> pathTaken) {
		String bestOpt = location;
		boolean x = true;
		double MinResist = 0;
		double resist;
		Node currLoc = getNode(location);
		ArrayList<Node> nghbs = currLoc.getNghb();
		for(Node nghb : nghbs) {
			if(pathTaken.contains(nghb.getLocation())) {
				continue;
			}
			if(nghb.isGoal()) {
				resist = -truepotential[currLoc.getIndex()-1]/amper;
			} else {
				resist = -(truepotential[currLoc.getIndex()-1] - truepotential[nghb.getIndex()-1]) / amper;
			}
			if(x) {
				MinResist = resist;
				bestOpt = nghb.getLocation();
				x = false;
			}
			else if(resist < MinResist) {
				MinResist = resist;
				bestOpt = nghb.getLocation();
			}
		}
		return bestOpt;
	}
	
	public boolean isWall(String location) {
		return walls.contains(location) || walls.contains(location.split("-")[1] + "-" + location.split("-")[0]);
	}
}
