package fax.projekt;

import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.*;

public class Mapper extends JFrame{

	GFX gfx;
	
	public Mapper(int col, int row, String start, String goal, ArrayList<String> wall, LinkedList<String> path) {
		this.setSize(200*col, 200*row);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.gfx = new GFX(col, row, start, goal, wall, path);
		this.add(gfx);
		this.setVisible(true);
	}

}
