package fax.projekt;

import java.util.ArrayList;

public class Node implements Comparable<Node>{
	private int row;
	private int column;
	private int index;
	private boolean active = false;
	private boolean goal = false;
	private ArrayList<Node> neighbours = new ArrayList<Node>();
	private int heuristic;
	
	
	public Node(int row, int column, int index) {
		this.row = row;
		this.column = column;
		this.index = index;
	}
	
	public int getRow() {
		return this.row;
	}
	
	public int getDistTo(String location) {
		int dist1 = Math.abs(Integer.valueOf(location.split(",")[0]) - row);
		int dist2 = Math.abs(Integer.valueOf(location.split(",")[1]) - column);
		return dist1+dist2;
	}
	
	public int getColumn() {
		return this.column;
	}
	
	public void setHeuristic(String goal) {
		int dist1 = Math.abs(Integer.valueOf(goal.split(",")[0]) - row);
		int dist2 = Math.abs(Integer.valueOf(goal.split(",")[1]) - column);
		this.heuristic = dist1 + dist2;
	}
	
	public int getHeuristic() {
		return this.heuristic;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public String getLocation() {
		return this.row + "," + this.column;
	}
	
	public void setGoal() {
		this.goal = true;
	}
	
	public boolean isGoal() {
		return this.goal;
	}
	
	public void setActive(boolean value) {
		this.active = value;
	}
	
	public boolean isActive() {
		return this.active;
	}
	
	public void addNghb(Node neighbour) {
		neighbours.add(neighbour);
	}
	
	public ArrayList<Node> getNghb() {
		return this.neighbours;
	}

	@Override
	public int compareTo(Node o) {
		return 0;
	}
	
}
