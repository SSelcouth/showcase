package fax.projekt;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.*;

public class GFX extends JPanel{
	int col;
	int row;
	String goal;
	String start;
	ArrayList<String> walls = new ArrayList<String>();
	LinkedList<String> path = new LinkedList<String>();
	int scale = 200;
	int offset = scale/2;
	
	public GFX(int col, int row, String start, String goal, ArrayList<String> walls, LinkedList<String> path) {
		this.col = col;
		this.row = row;
		this.start = start;
		this.goal = goal;
		this.walls.addAll(walls);
		for(String wae : path) {
			this.path.addLast(wae);
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.BLACK);
		
		Graphics2D g2D = (Graphics2D) g;
		
		g2D.setColor(Color.BLUE);
		g2D.setStroke(new BasicStroke(5));
		g2D.drawRect(2, 2, scale*col, scale*row);
		for(String wall : walls) {
			int y1 = Integer.valueOf(wall.split("-")[0].split(",")[0]);
			int x1 = Integer.valueOf(wall.split("-")[0].split(",")[1]);
			int y2 = Integer.valueOf(wall.split("-")[1].split(",")[0]);
			int x2 = Integer.valueOf(wall.split("-")[1].split(",")[1]);
			g2D.drawLine(x1*scale, y1*scale, (x2-1)*scale, (y2-1)*scale);
		}
		
		g2D.setColor(Color.YELLOW);
		int goalX = Integer.valueOf(goal.split(",")[1]);
		int goalY = Integer.valueOf(goal.split(",")[0]);
		g2D.drawOval(((goalX-1)*scale)+offset, (goalY*scale)-offset, 30, 30);
		
		boolean first = true;
		String curr;
		String past = null;
		g2D.setColor(Color.RED);
		for(String wae : path) {
			curr = wae;
			if(first) {
				past = curr;
				first = false;
			} else {
				int y1 = Integer.valueOf(past.split(",")[0]);
				int x1 = Integer.valueOf(past.split(",")[1]);
				int y2 = Integer.valueOf(curr.split(",")[0]);
				int x2 = Integer.valueOf(curr.split(",")[1]);
				
				g2D.drawLine((x1*scale)-offset, (y1*scale)-offset, (x2*scale)-offset, (y2*scale)-offset);
				past = curr;
			}
		}
	}
}
