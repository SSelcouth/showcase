import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Base64;

import javax.crypto.AEADBadTagException;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class PassManager {
	
	private static final String ALG = "AES/GCM/NoPadding";
	private static final int IV_SALT_LEN = 16;
	private static File storageFile;
	private static File saltFile;
	private static File ivFile;
//	private static final String dirPath = "/home/selcouth/Documents/Code/SRS/";
	private static final String dirPath = new File("").getAbsolutePath().concat("/");
	private static byte[] salt;
	private static byte[] iv;
    private final static Charset charset = StandardCharsets.UTF_8;
    private static final String storage = "storage.txt.8102";
    private static byte[] enc_key;
	
	private static byte[] encryptPassword(String password, int iterations, int length) 
			throws NoSuchAlgorithmException, InvalidKeySpecException {
		
		KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, iterations, length);
		
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");

		return factory.generateSecret(spec).getEncoded();
	}

    private static String encrypt(byte[] key, String rawData) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance(ALG);
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"), new GCMParameterSpec(128, iv));

            byte[] encrypted = cipher.doFinal(rawData.getBytes(charset));

            ByteBuffer byteBuffer = ByteBuffer.allocate(1 + iv.length + encrypted.length);
            byteBuffer.put((byte) iv.length);
            byteBuffer.put(iv);
            byteBuffer.put(encrypted);
            return Base64.getEncoder().encodeToString(byteBuffer.array());
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
	
	private static void init() throws IOException {
		System.out.println("Initialization START");
		
		storageFile.createNewFile();
		saltFile = new File(dirPath + "salt.enc");
		ivFile = new File(dirPath + "iv.enc");
		
		saltFile.createNewFile();
		ivFile.createNewFile();
		
		SecureRandom random = new SecureRandom();
		salt = new byte[IV_SALT_LEN];
		random.nextBytes(salt);
		FileOutputStream outSalt = new FileOutputStream(dirPath + "salt.enc");
		outSalt.write(salt);
		outSalt.close();
		
		System.out.println("Salt calculated");
		
		iv = new byte[IV_SALT_LEN];
		random.nextBytes(iv);
		FileOutputStream outIv = new FileOutputStream(dirPath + "iv.enc");
		outIv.write(iv);
		outIv.close();
		
		System.out.println("IV calculated");
		System.out.println("Initialization END");
	}
	
	private static void setup(String masterPass) throws Exception {
		if(masterPass == null) {
			printErr(3);
			System.exit(1);
		}
		
		FileInputStream inSalt = new FileInputStream(dirPath + "salt.enc");
		salt = inSalt.readAllBytes();
		inSalt.close();
		saltFile = new File(dirPath + "salt.enc");
		
		FileInputStream inIv = new FileInputStream(dirPath + "iv.enc");
		iv = inIv.readAllBytes();
		inIv.close();	
		ivFile = new File(dirPath + "iv.enc");

		byte[] encryptedPass = encryptPassword(masterPass, 65536, 256);
		
		enc_key = encryptedPass;
	}
	
	private static String decrypt(byte[] key, String encryptedData) throws Exception {
	        try {
	            ByteBuffer byteBuffer = ByteBuffer.wrap(Base64.getDecoder().decode(encryptedData));

	            int ivLength = byteBuffer.get();
	            byte[] iv = new byte[ivLength];
	            byteBuffer.get(iv);
	            byte[] encrypted = new byte[byteBuffer.remaining()];
	            byteBuffer.get(encrypted);

	            Cipher cipher = Cipher.getInstance(ALG);
	            cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, "AES"), new GCMParameterSpec(128, iv));
	            byte[] decrypted = cipher.doFinal(encrypted);


	            return new String(decrypted, charset);
	        } catch (AEADBadTagException e) {
	        	throw new Exception("SECURITY ERROR: Could not decrypt - Tag mismatch!\nWrong master password.");
	        } catch (IllegalArgumentException e) {
	        	throw new Exception("SECURITY ERROR: Could not decrypt - Wrong ending unit!\\nWARNING - Data may have been tampered with"
	        			+ "\nPurging bad data recommended!");
	        } catch (Exception e) { //ignore this SO
	            throw new Exception("ERROR: UNKNOWN\nCould not decrypt", e);
	        }
	    }
	
	private static void managerPut(String masterPass, String address, String pass) throws Exception {
		setup(masterPass);
			
		String data2enc = address + "::" + pass;
		BufferedReader reader = new BufferedReader(new FileReader(dirPath + storage));
		BufferedWriter writer = new BufferedWriter(new FileWriter(dirPath + storage, true));
		String line;
		ArrayList<String> fileLines = new ArrayList<String>();
		boolean rewrite = false;
		boolean first = true;
		while(true) {
			line = reader.readLine();
			if(first) {
				first = false;
				continue;
			}
			if(line != null) {
				String[] dec_line = decrypt(enc_key, line).split("::");
				if(dec_line[0].equals(address)) {
					fileLines.add(encrypt(enc_key, data2enc));
					rewrite = true;
				} else {
					fileLines.add(line);
				}
			} else {
				if(rewrite) {
					reader.close();
					writer.close();
					Files.delete(Paths.get(dirPath + storage));
					storageFile.createNewFile();
					writer = new BufferedWriter(new FileWriter(dirPath + storage));
					reader = new BufferedReader(new FileReader(dirPath + storage));
					for(String fileLine : fileLines) {
						System.out.println(fileLine);
						writer.newLine();
						writer.append(fileLine);
					}
					System.out.println("Password updated successfully.");
					break;
				} else {					
					String enc_data = encrypt(enc_key, data2enc);
					writer.newLine();
					writer.append(enc_data);
					System.out.println("Password added successfully.");
					break;
				}
			}
		}
		writer.close();
		reader.close();
		
	}
	
	private static void printErr(int errNum) {
		switch(errNum) {
			
		case 0:
			System.out.println("ERROR: Manager already initialized!");
			break;
			
		case -1:
			System.out.println("ERROR: Manager not initialized. Please initialize the manager first");
			break;
			
		case -2:
			System.out.println("SYNTAX ERROR: Command not recognized.");
			break;
			
		case -3:
			System.out.println("SYNTAX ERROR: Argument not recognized.");
			break;
			
		case 1:
			System.out.println("SYNTAX ERROR: Missing arguments.");
			break;
			
		case 2:
			System.out.println("SYNTAX ERROR: Too many arguments.");
			break;
			
		case 404:
			System.out.println("ERROR: Address not found in database.");
			break;
		
		default:
			System.out.println("ERROR: UNKNOWN.");
			break;
		}
		
		System.out.println("Run 'java PassManager help' for more info.");
		System.exit(0);
	}
	
	private static void managerGet(String masterPass, String address) throws Exception {
		setup(masterPass);
		
		String line;
		BufferedReader reader = new BufferedReader(new FileReader(dirPath + storage));
		boolean first = true;
		while(true) {
			line = reader.readLine();
			if(first) {
				first = false;
				continue;
			}
			if(line != null) {
				String dec_line[] = decrypt(enc_key, line).split("::");
				if(dec_line[0].equals(address)) {
					System.out.println("The password for " + address + " is: " + dec_line[1]);
					break;
				}
				
			} else {
				printErr(404);
				break;
			}
		}
		reader.close();
	}
	
	private static void printHelp() {
		System.out.println("Welcome to N's PasswordManager!\n"
				+ "To use the manager, type in commands in the form: java PassManger [COMMAND] [MASTERPASSWORD] [ARGS]\n"
				+ "There are four commands in total\n"
				+ "\nFirstly, run the initialization command like so:\njava PassManger init [MASTERPASSWORD]\n"
				+ "\nTo store a password run:\njava PassManger put [MASTERPASSWORD] [ADDRESS] [PASSWORD]\n"
				+ "\nTo retrieve a password run:\njava PassManger get [MASTERPASSWORD] [ADDRESS]\n"
				+ "\nFor help run:\njava PassManger help\n"
				+ "\nThank you for using N's PasswordManager! Have a safe day!");
	}
	
	private static void purgeData(String masterPass) throws Exception {
		setup(masterPass);
		
		saltFile.delete();
		ivFile.delete();
		storageFile.delete();
		System.out.println("All data purged. Reinitialization of PassManager required.");
	}
	
	public static void main(String[] args) throws Exception {
		
		storageFile = new File(dirPath + storage);
		switch(args[0]) {
		
			case "init":
				if(storageFile.exists())
					printErr(0);
				else
					init();
				break;
				
			case "put":
				if(storageFile.exists()) {
					if(args.length < 4)
						printErr(1);
					else if(args.length > 4)
						printErr(2);
					managerPut(args[1], args[2], args[3]);
				} else
					printErr(-1);
				break;
				
			case "get":
				if(storageFile.exists()) {
					if(args.length < 3)
						printErr(1);
					else if(args.length > 3)
						printErr(2);
					managerGet(args[1], args[2]);
				} else
					printErr(-1);
				break;
				
			case "help":
				printHelp();
				break;
				
			case "purge":
				if(storageFile.exists()) {
					if(args.length < 2)
						printErr(1);
					else if(args.length > 2)
						printErr(2);
					purgeData(args[1]);
				} else
					printErr(-1);
				break;
				
			default:
				printErr(-2);
				break;
				
		}
		
	}

}

