#!/bin/bash
echo "Prevodenje zapoceto"
javac PassManager.java
echo "Prevodenje zavrseno"
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Prilikom prvog pokretanja, Manager odbija bilo kakvu operaciju osim inicijalizacije"
echo ">>java PassManager put masterPass gmail.com 12345"
java PassManager put masterPass123 gmail.com 12345
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Stoga pokrecemo inicijalizaciju"
echo ">>java PassManager init"
java PassManager init
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Jednom kad smo je pokrenuli, inicijalizaciju nemozemo pokretati ponovno (osim ako ne pobrisemo sve podatke)"
echo ">>java PassManager init"
java PassManager init
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

var='help'
echo "Za pravila sintakse trazimo pomoc"
echo ">>java PassManager help"
java PassManager $var
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Manager javlja greske poput nedostatka, viska ili nepoznatih argumenata, te nepoznatih komandi"
echo ">>java PassManager put nesto"
java PassManager put nesto
echo " "
echo ">>java PassManager put masterPass gmail.com password excessData"
java PassManager put masterPass123 gmail password exceessData
echo " "
echo ">>java PassManager weirdCommand masterPass"
java PassManager weirdCommand masterPass
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Dodati cemo nekoliko podataka"
echo ">>java PassManager put masterPass123 gmail.com simplePassword"
java PassManager put masterPass123 gmail.com simplePassword
echo " "
echo ">>java PassManager put masterPass123 yahoo.com notSOs1mpl3P@55w0rD##"
java PassManager put masterPass123 yahoo.com notSos1mple3P@55w0rD##
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Provjeriti cemo podatke"
echo ">>java PassManager get masterPass123 gmail.com"
java PassManager get masterPass123 gmail.com
echo " "
echo ">>java PassManager get masterPass123 yahoo.com"
java PassManager get masterPass123 yahoo.com
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Zaporka za gmail.com je slaba. Promijeniti cemo je"
echo ">>java PassManager put masterPass123 gmail.com MAL0B0LJAZ@P0RK@"
java PassManager put masterPass123 gmail.com MAL0B0LJAZ@P0RK@
echo " "
echo ">>java PassManager get masterPass123 gmail.com"
java PassManager get masterPass123 gmail.com
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Pri koristenju krive glavne zaporke, Manager javlja pogresku"
echo ">>java PassManager get wrongMaster321 gmail.com"
java PassManager get wrongMaster321 gmail.com
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "U slucaju da je napadac promijeno sadrzaj spremista, ili dodao nove linije, dobivamo gresku"
echo ">>file=storage.txt.8102"
echo ">>echo 'jaowifjWDOJWAs231WDo2e21dJODWA29dJ2d0sWd20' >> file"
echo ">>java PassManager put masterPass123 twitter.com nekiPasssword"
file='storage.txt.8102'
echo "jaowifjWDOJWAs231WDo2e21dJODWA29dJ2d0sWd20" >> $file
java PassManager put masterPass123 twitter.com nekiPassword
read -n 1 -p "Prisnite Enter za dalje": nekivar
echo "--------------------------------------------------------------------------------------------------"

echo "Program, nazalost, nema samokorekcijsku opciju. U slucaju da je napadac promijenio sadrzaj ili izbrisao neku od datoteka, jedina opcija je resetiranje Managera"
echo ">>java PassManager purge masterPass123"
java PassManager purge masterPass123
echo "Kraj demonstracije"
