package fax.math.labosi;

import java.util.Scanner;

public class Rekurzija {

	
	public static float determinant(float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3) {
		float rj;
		float p1 = a1 * (b2 * c3 - b3 * c2);
		float p2 = a2 * (b1 * c3 - b3 * c1);
		float p3 = a3 * (b1 * c2 - b2 * c1);
		rj = p1 - p2 + p3;
		return rj;
	}
	
	
	public static double prviNacin(float a2, float b2, float c2, float d1, float d2, float d3,int n) {
		double rj;
		float a1 = 1;
		float b1 = 1;
		float c1 = 1;
		float a3 = a2 * a2;
		float b3 = b2 * b2;
		float c3 = c2 * c2;
		
		float delta = determinant(a1, a2, a3, b1, b2, b3, c1, c2, c3);
		float Lam1 = determinant(d1, d2, d3, b1, b2, b3, c1, c2, c3) / delta;
		float Lam2 = determinant(a1, a2, a3, d1, d2, d3, c1, c2, c3) / delta;
		float Lam3 = determinant(a1, a2, a3, b1, b2, b3, d1, d2, d3) / delta;
		
		double x1 = Math.pow(a2, n);
		double x2 = Math.pow(b2, n);
		double x3 = Math.pow(c2, n);
		
		rj = Lam1 * x1 + Lam2 * x2 + Lam3 * x3;
		
		return rj;
	}
	
	public static float rek(float[] K, int n, float[] A) {
		if(n == 0)
			return A[0];
		else if (n == 1)
			return A[1];
		else if (n == 2)
			return A[2];
		else {
			return K[0] * rek(K, n-1, A) + K[1] * rek (K, n-2, A) + K[2] * rek(K, n-3, A);
		}
		
	}
	
	public static float drugiNacin(float x0, float x1, float x2, int n, float a_0, float a_1, float a_2) {
		float rj;
		float a1 = x0 * x0;
		float a2 = x1 * x1;
		float a3 = x2 * x2;
		float d1 = a1 * x0;
		float d2 = a2 * x1;
		float d3 = a3 * x2;
		float K[] = new float[3];
		float delta = determinant(a1, a2, a3, x0, x1, x2, 1, 1, 1);
		K[0] = determinant(d1, d2, d3, x0, x1, x2, 1, 1, 1) / delta;
		K[1] = determinant(a1, a2, a3, d1, d2, d3, 1, 1, 1) / delta;
		K[2] = determinant(a1, a2, a3, x0, x1, x2, d1, d2, d3) / delta;
		float A[] = new float[3];
		A[0] = a_0;
		A[1] = a_1;
		A[2] = a_2;
		rj = rek(K, n, A);
		
		return rj;
	}
		
	
	public static void main(String[] args) {
		float x_0, x_1, x_2;
		float a_0, a_1, a_2;
		int n;
		double rj_1;
		float rj_2;
		
		try(Scanner sc = new Scanner(System.in)) {
			System.out.print("Unesite prvo rjesenje x_0 karakteristicne jednadzbe: ");
			x_0 = sc.nextFloat();
			System.out.print("Unesite drugo rjesenje x_1 karakteristicne jednadzbe: ");
			x_1 = sc.nextFloat();
			System.out.print("Unesite trece rjesenje x_2 karakteristicne jednadzbe: ");
			x_2 = sc.nextFloat();
			System.out.print("Unesite vrijednost nultog clana niza a_0: ");
			a_0 = sc.nextFloat();
			System.out.print("Unesite vrijednost prvog clana niza a_1: ");
			a_1 = sc.nextFloat();
			System.out.print("Unesite vrijednost drugog clana niza a_2: ");
			a_2 = sc.nextFloat();
			System.out.print("Unesite redni broj n trazenog clana niza: ");
			n = sc.nextInt();
		}
		
		rj_1 = prviNacin(x_0, x_1, x_2, a_0, a_1, a_2, n);
		System.out.println("Vrijednost n-tog clana pomocu formule: " + rj_1);
		rj_2 = drugiNacin(x_0, x_1, x_2, n, a_0, a_1, a_2);
		System.out.println("Vrijednost n-tog clana rekurzivno: " + rj_2);
		
	}

}
