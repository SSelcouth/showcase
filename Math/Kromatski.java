import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;

public class Kromatski {
	
	static class Vertex implements Comparable<Vertex>, Comparator<Vertex>{
		int number;
		int colour;
		LinkedList<Vertex> neighbours;
		Integer rang;
		
		public Vertex(int number) {
			this.number = number;
			this.colour = 0;
			this.neighbours = new LinkedList<Vertex>();
		}
		
		public int getColour() {
			return this.colour;
		}
		
		public int getNumber() {
			return this.number;
		}
		
		public LinkedList<Vertex> getNghb() {
			return this.neighbours;
		}
		
		public boolean isColourLegal(int colour) {
			for(Vertex nghb : neighbours) {
				if(nghb.getColour() == colour) {
					return false;
				}
			}
			return true;
		}
		
		public void setColour(int colour) {
			this.colour = colour;
		}
		
		public void addNghb(Vertex nghb) {
			this.neighbours.add(nghb);
		}
		
		public void updateRang() {
			this.rang = Integer.valueOf(this.neighbours.size());
		}
		
		public Integer getRang() {
			return this.rang;
		}

		@Override
		public int compareTo(Vertex arg0) {
			return arg0.getRang().compareTo(this.getRang());
		}

		@Override
		public int compare(Vertex arg0, Vertex arg1) {
			return arg0.getRang() - arg1.getRang();
		}
		
		@Override
		public String toString() {
			return "Vertex: " + number + "\n	Colour: " + colour;
		}
		
		public int getLowestColour() {
			for(int i = 1; i <= 15; i++) {
				if(this.isColourLegal(i)) {
					return i;
				}
			}
			return 0;
		}
		
		public void colourNghbs(int newColour) {
			for(Vertex nghb : neighbours) {
				if(nghb.getColour() == 0) {
					nghb.setColour(newColour);
				}
			}
		}
		
	}

	public static void main(String[] args) {
		int NofVertex;
		LinkedList<Vertex> vertices = new LinkedList<Vertex>();
		LinkedList<String> rules = new LinkedList<String>();
		String tmp;
		ArrayList<Integer> colours = new ArrayList<Integer>();
		String fileN;
		
			try(Scanner sc = new Scanner(System.in)) {
				System.out.print("Unesite ime datoteke: ");
				fileN = sc.nextLine();
			}
			
			//Nije mi jasan taj dio sa datotekom, gdje bi se ona trebala nalazit?
			//Bi li program trebao traziti datoteku po racunalu?
			//Ili je u istom direktoriju kao i kod?
			//Ili u direktoriju iz kojeg IDE povlaci resurse?
			
			String dirPath = new File("").getAbsolutePath().concat("/"+fileN);
			
			File file = new File(dirPath);
		
		
		//Read the matrix
		try(Scanner sc = new Scanner(file)) {
			NofVertex = Integer.valueOf(sc.nextLine());
			for(int i = 0; i < NofVertex; i++) {
				vertices.addLast(new Vertex(i));
			}
			while(sc.hasNext()) {
				tmp = sc.nextLine();
				if(tmp.isBlank()) {
					continue;
			//	} else if(tmp.equals("done")) {
			//		break;
				} else {
					rules.addLast(tmp);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//Fill out neighbours
		for(Vertex vertex : vertices) {
			String nghbs[] = rules.pop().split(" ");
			for(int i = 0; i < nghbs.length; i++) {
				if(nghbs[i].equals("1")) {
					vertex.addNghb(vertices.get(i));
				}
			}
			vertex.updateRang();
		}
		
		//Sort the vertices by rang
		Collections.sort(vertices);

		for(Vertex vertex : vertices) {
			int newColour = vertex.getLowestColour();
			int tmpCol = newColour;
			//if uncoloured
			if(vertex.getColour() == 0) {
				//add lowest legal colour
				vertex.setColour(newColour);
				//one-up blank neighbours
				vertex.colourNghbs(vertex.getColour()+1);
			//if coloured
			} else {
				//...legally, deal with neighbours
				if(vertex.isColourLegal(vertex.getColour())) {
					if(vertex.getColour() == 1) {
						vertex.colourNghbs(vertex.getColour()+1);
					} else {
						vertex.colourNghbs(vertex.getColour()-1);
					}
				//...illegally 
				} else {
					//find lowest legal colour
					do {
						tmpCol++;
					} while(!vertex.isColourLegal(tmpCol));
					//colour self and one-up neighbours
					vertex.setColour(tmpCol);
					vertex.colourNghbs(vertex.getColour()+1);
				}
			}
		}
		for(Vertex vertex : vertices) {
			if(!colours.contains(Integer.valueOf(vertex.getColour()))) {
				colours.add(Integer.valueOf(vertex.getColour()));
			}
		}
		System.out.println(colours.size());
	}
}



