import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Ciklus {
	
	static class Vertex {
		private int number;
		private LinkedList<Integer> nghbrs = new LinkedList<Integer>();
		
		public Vertex(int number) {
			this.number = number;
		}
		
		public void addNghb(int number) {
			nghbrs.addLast(Integer.valueOf(number));
		}
		
		public LinkedList<Integer> getNghb() {
			return nghbrs;
		}
		
		public int getNumber() {
			return number;
		}
		
	}
	
	static class Cycle {
		private LinkedList<Vertex> thePath = new LinkedList<Vertex>();
		String rawPath;
		boolean isOver;
		
		public Cycle(Vertex newV) {
			this.thePath.addLast(newV);
			this.rawPath = String.valueOf(newV.getNumber());
			this.isOver = false;
		}
		
		public Cycle(Vertex firstV, Vertex nextV) {
			this.thePath.addLast(firstV);
			this.thePath.addLast(nextV);
			this.rawPath = String.valueOf(firstV.getNumber()) + "-" + String.valueOf(nextV.getNumber());
			this.isOver = false;
		}
		public Cycle(Cycle copy, Vertex nextV) {
			this.thePath.addAll(copy.getPath());
			this.rawPath = copy.getRawPath();
			rawPath = rawPath + "-" + String.valueOf(nextV.getNumber());
			thePath.addLast(nextV);
			this.isOver = false;
		}
		
		public Cycle(Cycle copy) {
			this.thePath.addAll(copy.getPath());
			this.rawPath = copy.getRawPath();
			this.isOver = false;
		}
		
		public void addV(Vertex nextV) {
			thePath.addLast(nextV);
			rawPath = rawPath + "-" + String.valueOf(nextV.getNumber());
		}
		
		public void copy(LinkedList<Vertex> newP, String newRaw) {
			thePath.clear();
			thePath.addAll(newP);
			rawPath = newRaw;
			
		}
		
		public Vertex getFinal() {
			return thePath.getLast();
		}
		
		public LinkedList<Vertex> getPath() {
			return thePath;
		}
		
		public String getRawPath() {
			return rawPath;
		}
		
		public void Finnish() {
			isOver = true;
		}
		
		public boolean isOver() {
			return isOver;
		}
	}
	
	public static void main(String[] args) {
		LinkedList<Vertex> vertices = new LinkedList<Vertex>();
		LinkedList<String> rules = new LinkedList<String>();
		ArrayList<String> edges = new ArrayList<String>();
		int NofVertex = 0;
		String tmp;
		String fileN;
		
		try(Scanner sc = new Scanner(System.in)) {
			System.out.print("Unesite ime datoteke: ");
			fileN = sc.nextLine();
		}
		
		//Nije mi jasan taj dio sa datotekom, gdje bi se ona trebala nalazit?
		//Bi li program trebao traziti datoteku po racunalu?
		//Ili je u istom direktoriju kao i kod?
		//Ili u direktoriju iz kojeg IDE povlaci resurse?
		
		String dirPath = new File("").getAbsolutePath().concat("/"+fileN);
		
		File file = new File(dirPath);
		
		
		//Read
		try(Scanner sc = new Scanner(file)) {
			NofVertex = Integer.valueOf(sc.nextLine());
			for(int i = 0; i < NofVertex; i++) {
				vertices.addLast(new Vertex(i));
			}
			while(sc.hasNext()) {
				tmp = sc.nextLine();
				if(tmp.isBlank()) {
					continue;
				} else {
					rules.addLast(tmp);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//Fill out neighbours
		for(Vertex vertex : vertices) {
			String nghbs[] = rules.pop().split(" ");
			for(int i = 0; i < nghbs.length; i++) {
				if(nghbs[i].equals("1")) {
					vertex.addNghb(i);
					if(!edges.contains(vertex.getNumber() + "," + (i)) && !edges.contains((i) + "," + vertex.getNumber())) {
						edges.add(vertex.getNumber() + "," + (i));
					}
				}
			}
			
		} 
		
		int maxCycle = 0;
		//Start finding cycles
		for(Vertex vertex : vertices) {
			ArrayList<Cycle> cycles = new ArrayList<Cycle>();
			//setup
			Vertex start = vertex;
			Vertex currVer = start;
			Vertex nextVer;
			boolean allDone;
			//isolated node case
			if(start.getNghb().isEmpty()) {
				continue;
			}
			for(int nghb : currVer.getNghb()) {
				cycles.add(new Cycle(currVer, vertices.get(nghb)));
			}
			
			do {
				
				for(int i = 0; i < cycles.size(); i++) {
					Cycle cycle = cycles.get(i);
					currVer = cycle.getFinal();
					boolean first = true;
					boolean doneSomething = false;
					Cycle tmpC = new Cycle(cycle);
					for(int nghb : currVer.getNghb()) {
						nextVer = vertices.get(nghb);
						//no repeating vertices
						if(cycle.getPath().contains(nextVer)) {
							continue;
						} else {
							//prolong path
							if(first) {
								tmpC.addV(nextVer);
								first = false;
								doneSomething = true;
							//make new paths if multiple possibilities exist
							} else {
								cycles.add(new Cycle(cycle, nextVer));
								doneSomething = true;
							}
						}
					}
					cycle.copy(tmpC.getPath(), tmpC.getRawPath());
					//dead end
					if(!doneSomething) {
						cycle.Finnish();
					}
				}
				//check if there's more options
				allDone = true;
				for(Cycle cycle : cycles) {
					if(!cycle.isOver()) {
						allDone = false;
						break;
					}
				}
			} while(!allDone);
			//if it is a true cycle, check length
			for(Cycle cycle : cycles) {
				if(cycle.getFinal().getNghb().contains(vertices.indexOf(start)) && cycle.getPath().size() > 2) {
					if(cycle.getPath().size() > maxCycle) {
						maxCycle = cycle.getPath().size();
					}
				}
			}
		}
		System.out.println(maxCycle);
	}
	
}
