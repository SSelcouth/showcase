import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class GameOfLife extends PApplet {

Game test;

public void setup() {
  
  test = new Game(4, width, height);
}

public void draw() {
  test.show();
  test.nextGen();
}

class Game {
  
  int[][] grid;
  int cols;
  int rows;
  int resolution;
  int[][] nextGrid;
  int lastcol;
  int lastrow;
  
  Game(int resolution, int w, int h) {
    this.resolution = resolution;
    this.cols = w / resolution;
    this.rows = h / resolution;
    this.lastcol = cols-1;
    this.lastrow = rows-1;
    nextGrid = new int[cols][rows];
    grid = new int[cols][rows];
    for(int i = 0; i < cols; i++) {
      for(int j = 0; j < rows; j++) {
        if(i >= (int) cols/3 && i <= (int) cols*2/3 && j >= (int) rows/3 && j <= (int) rows*2/3) {
          if((int) random(4) == 0) {
            grid[i][j] = 1;
          } else {
            grid[i][j] = 0;
          }
        }
      }
    }
  }
  
  public void show() {
    background(0);
    for(int i = 0; i < cols; i++) {
      for(int j = 0; j < rows; j++) {
        int x = i * resolution;
        int y = j * resolution;
        if(grid[i][j] == 1) {
          fill(255);
          stroke(0);
          rect(x, y, resolution-1, resolution-1);
        }
      }
    }
  }
  
  public int getNghbSum(int x, int y) {
    int sum = 0;
    for(int i = -1; i < 2; i++) {
      for(int j = -1; j < 2; j++) {
        if((x+i) == -1 && (y+j) == -1) {
          sum += grid[lastcol][lastrow];
        } else if((x+i) == -1) {
          sum += grid[lastcol][(y+j)%rows];
        } else if((y+j) == -1) {
          sum += grid[(x+i)%cols][lastrow];
        } else {
          sum += grid[(x+i)%cols][(y+j)%rows];
        }
      }
    }
    sum -= grid[x][y];
    return sum;
  }
        
          
  public void nextGen() {
    for(int i = 0; i < cols; i++) {
      for(int j = 0; j < rows; j++) {
        int sum = getNghbSum(i, j);
        //int sum = (int) random(0,8);
        if(grid[i][j] == 0) {
          if(sum == 3) {
            nextGrid[i][j] = 1;
          } else {
            nextGrid[i][j] = 0;
          }
        } else {
          if(sum < 2 || sum > 3) {
            nextGrid[i][j] = 0;
          } else {
            nextGrid[i][j] = 1;
          }
        }
      }
    }
    for(int i = 0; i < cols; i++) {
      for(int j = 0; j < rows; j++) {
        grid[i][j] = nextGrid[i][j];
      }
    }
  }
    
}
  public void settings() {  size(1920, 1200); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "GameOfLife" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
