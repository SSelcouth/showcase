package ui;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		String testPath = null;
		String trainPath = null;
		String nn[] = null;
		int iter = 0;
		double K = 0;
		double p = 0;
		int popsize = 0;
		int elitism = 0;
		int check = -1;
		
		
		for(int i = 0; i < args.length; i++) {
			switch(args[i]) {
			case "--train":
				trainPath = args[++i];
				break;
			case "--test":
				testPath = args[++i];
				break;
			case "--nn":
				nn = args[++i].split("s");
				break;
			case "--iter":
				iter = Integer.valueOf(args[++i]);
				break;
			case "--K":
				K = Double.valueOf(args[++i]);
				break;
			case "--p":
				p = Double.valueOf(args[++i]);
				break;
			case "--popsize":
				popsize = Integer.valueOf(args[++i]);
				break;
			case "--elitism":
				elitism = Integer.valueOf(args[++i]);
				break;
			case "--check":
				check = Integer.valueOf(args[++i]);
				break;
				
			}
		}
		
		if(check == -1) {
			check = 2000;
		}
		
		int trueNN[] = new int[nn.length];
		for(int i = 0; i < trueNN.length; i++) {
			trueNN[i] = Integer.valueOf(nn[i]);
		}
		
		
		File testFile = new File(testPath);
		File trainFile = new File(trainPath);
		LinkedList<LinkedList<Neuron>> neuronLayers = new LinkedList<LinkedList<Neuron>>();
		
		LinkedList<String> input = new LinkedList<String>();
		
		try(Scanner sc = new Scanner(trainFile)) {
			String tmp[] = sc.nextLine().split(",");
			int inLen = tmp.length - 1;
			for(int i = 0; i < trueNN.length; i++) {
				neuronLayers.add(new LinkedList<Neuron>());
				int layerIndex = neuronLayers.size() - 1;
				int len;
				if(layerIndex == 0) {
					len = inLen;
				} else {
					len = neuronLayers.get(layerIndex-1).size();
				}
				for(int j = 0; j < trueNN[i]; j++) {
					neuronLayers.get(layerIndex).add(new Neuron(len));
				}
			}
			while(sc.hasNext()) {
				input.add(sc.nextLine());
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		LinkedList<String> testInput = new LinkedList<String>();
		
		try(Scanner sc = new Scanner(testFile)) {
			sc.nextLine();
			while(sc.hasNext()) {
				testInput.add(sc.nextLine());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		Neuron finalNeuron = new Neuron(neuronLayers.get(neuronLayers.size()-1).size());
		neuronLayers.add(new LinkedList<Neuron>());
		neuronLayers.get(neuronLayers.size()-1).add(finalNeuron);
		NeuralNet network = new NeuralNet(neuronLayers, input);
		Population testPopulation = new Population(popsize, elitism, p, K, network, iter, check);
		testPopulation.genAlg();
		testPopulation.test(testInput);
	}

}
