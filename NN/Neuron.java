package ui;

import java.util.Random;

public class Neuron {

	double output;
	double bias[];
	
	public Neuron(int len) {
		this.bias = new double[len+1];
	}
	
	public Neuron(double bias[]) {
		this.bias = bias;
	}
	
	public Neuron(Neuron neuron) {
		this.bias = new double[neuron.getBias().length];
	}
	
	public void randomizeBias() {
		Random random = new Random();
		for(int i = 0; i < bias.length; i++) {
			bias[i] = random.nextFloat();
		}
	}
	
	public double net(double in[]) {
		double net = 0;
		for(int i = 0, b = 1; i < in.length; i++, b++) {
				net += in[i] * bias[b];
		}
		return net + bias[0];
	}
	
	public void setBias(double newBias[]) {
		this.bias = newBias;
	}
	
	public double[] getBias() {
		return this.bias;
	}
	
}
