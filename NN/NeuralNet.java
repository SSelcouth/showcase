package ui;

import java.util.LinkedList;
import java.util.Random;


public class NeuralNet implements Comparable<NeuralNet>{

	int generation;
	LinkedList<LinkedList<Neuron>> layers;
	double expectedResults[];
	LinkedList<double[]> input = new LinkedList<double[]>();
	double error;
	int N;
	double[] output;
	int totalBiasNumber;
	int number;
	
	public NeuralNet(LinkedList<LinkedList<Neuron>> lays, LinkedList<String> input) {
		this.layers = lays;
		error = 0;
		N = input.size();
		parseInput(input);
		this.output = new double[input.size()];
		this.number = -1;
	}
	
	public NeuralNet(NeuralNet template, int number) {
		this.number = number;
		this.error = 0;
		this.N = template.getN();
		this.output = template.getOutput();
		this.input = template.getInput();
		this.expectedResults = template.getExpectedResults();
		this.layers = new LinkedList<LinkedList<Neuron>>();
		for(LinkedList<Neuron> layer : template.getLayers()) {
			layers.add(new LinkedList<Neuron>());
			for(Neuron neuron : layer) {
				layers.getLast().add(new Neuron(neuron));
			}
		}
	}
	
	public LinkedList<LinkedList<Neuron>> getLayers() {
		return this.layers;
	}
	
	public int getN() {
		return this.N;
	}
	
	public double[] getOutput() {
		return this.output;
	}
	
	public LinkedList<double[]> getInput() {
		return this.input;
	}
	
	public double[] getExpectedResults() {
		return this.expectedResults;
	}
	
	public double sigm(double net) {
		double y = 0;
		y = 1 / (1 + Math.pow(Math.E, -net));
		return y;
	}
	
	public double getErr() {
		return error / (double) N;
	}
	
	public double getPureErr() {
		return error;
	}
	
	public void err(double t, double o) {
		error += Math.pow((t - o), 2);
	}
	
	public void parseInput(LinkedList<String> in) {
		this.expectedResults = new double[in.size()];
		input.clear();
		for(String line : in) {
			String tmp[] = line.split(",");
			expectedResults[in.indexOf(line)] = Double.valueOf(tmp[tmp.length-1]);
			input.add(new double[tmp.length-1]);
			for(int i = 0; i < tmp.length-1; i++) {
				input.getLast()[i] = Double.valueOf(tmp[i]);
			}
		}
		this.output = new double[in.size()];
	}
	
	public void fit() {
		error = 0;
		double[] out;
		for(double[] in : input) {
			out = null;
			boolean first = true;
			double[] secretIn = null;
			for(LinkedList<Neuron> layer : layers) {
				if(layers.indexOf(layer) != 0) {
					secretIn = out;
				}
				out = new double[layer.size()];
				int i = 0;
				for(Neuron neuron : layer) {
					if(first) {
						out[i++] = neuron.net(in);
					} else {
						out[i++] = neuron.net(secretIn);
					}
				}
				first = false;
				if(layers.indexOf(layer) != layers.size()-1) {
					for(i = 0; i < out.length; i++) {
						out[i] = sigm(out[i]);
					}
				}
			}
			int index = input.indexOf(in);
			output[index] = out[0];
			err(expectedResults[index], output[index]);
		}
	}
	
	public void generateBiases() {
		Random random = new Random();
		for(LinkedList<Neuron> layer : layers) {
			for(Neuron neuron : layer) {
				double[] biases = new double[neuron.getBias().length];
				for(int i = 0; i < biases.length; i++) {
					biases[i] = random.nextGaussian() + 0.01;
				}
				neuron.setBias(biases);
			}
		}
	}
	
	public void setBiases(double[] biases) {
		int i = 0;
		for(LinkedList<Neuron> layer : layers) {
			for(Neuron neuron : layer) {
				double[] newbias = new double[neuron.getBias().length];
				for(int j = 0; j < newbias.length; j++) {
					newbias[j] = biases[i++];
				}
				neuron.setBias(newbias);
			}
		}
	}
	
	public void printOutput() {
		for(int i = 0; i < output.length; i++) {
			System.out.println(expectedResults[i] + " / " + output[i]);
		}
		System.out.println("Error: " + getErr() + " / Fitness: " + (1 / getErr()));
	}
	
	public double[] getNetworkBias() {
		totalBiasNumber = 0;
		for(LinkedList<Neuron> layer : layers) {
			for(Neuron neuron : layer) {
				totalBiasNumber += neuron.getBias().length;
			}
		}
		double[] nBiases = new double[totalBiasNumber];
		int i = 0;
		for(LinkedList<Neuron> layer : layers) {
			for(Neuron neuron : layer) {
				for(int j = 0; j < neuron.getBias().length; j++) {
					nBiases[i++] = neuron.getBias()[j];
				}
			}
		}
		return nBiases;
	}
	
	public void printSelf() {
		for(LinkedList<Neuron> layer : layers) {
			for(Neuron neuron : layer) {
				double[] biases = neuron.getBias();
				for(int i = 0; i < biases.length; i++) {
					System.out.format("W%d = %.4f ", i, biases[i]);
				}
				System.out.println();
			}
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null || this.getClass() != obj.getClass())
			return false;
		
		NeuralNet nn = (NeuralNet) obj;
		return nn.getNetworkBias().length == this.getNetworkBias().length && nn.getNetworkBias() == this.getNetworkBias();
	}
	
	@Override
	public int compareTo(NeuralNet arg0) {
		if(arg0.getErr() > this.getErr())
			return -1;
		else
			return 1;
	}
	
	
}
