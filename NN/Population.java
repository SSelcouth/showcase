package ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

public class Population {

	ArrayList<NeuralNet> population;
	int elitism;
	double mutationOdds;
	double mutationScale;
	int generation;
	ArrayList<NeuralNet> nextGeneration;
	int nofIter;
	NeuralNet template;
	int popsize;
	int checkInterval;
	
	public Population(int popsize, int elitism, double mutationProbs, double mutationScale, NeuralNet template, int nOfIter, int check) {
		this.population = new ArrayList<NeuralNet>();
		this.elitism = elitism;
		this.mutationOdds = mutationProbs;
		this.mutationScale = mutationScale;
		this.generation = 0;
		this.checkInterval = check;
		this.template = template;
		this.nofIter = nOfIter;
		this.popsize = popsize;
		this.nextGeneration = new ArrayList<NeuralNet>();
		for(int i = 0; i < popsize; i++) {
			population.add(new NeuralNet(template, population.size()));
			population.get(population.size()-1).generateBiases();
		}
	}
	
	private void elitism() {
		Collections.sort(population);
		for(int i = 0; i < elitism; i++) {
			nextGeneration.add(population.get(i));
		}
	}
	
	private void fitness() {
		for(NeuralNet nn : population) {
			nn.fit();
		}
	}
	
	private NeuralNet select() {
		double fitnessSum = 0;
		for(NeuralNet nn : population) {
			fitnessSum += (1 / nn.getErr());
		}
		
		Random random = new Random();
		double pick = Math.abs(random.nextLong()) % fitnessSum;
		double segment = 0;
		for(NeuralNet nn : population) {
			segment += (1 / nn.getErr());
			if(pick <= segment) {
				return nn;
			}
		}
		return population.get(0);
	}
	
	private double[] mutate(double[] kidsBias) {
		double[] mutatedBias = new double[kidsBias.length];
		Random random = new Random();
		for(int i = 0; i < kidsBias.length; i++) {
			double diceRoll = ((double) random.nextInt() % 100) / 100;
			if(diceRoll <= mutationOdds) {
				mutatedBias[i] = kidsBias[i] + random.nextGaussian() + mutationScale;
			}
			else
				mutatedBias[i] = kidsBias[i];
		}
		return mutatedBias;
	}
	
	private void crossBreed(NeuralNet nnOne, NeuralNet nnTwo) {
		double[] fathersBias = nnOne.getNetworkBias();
		double[] momsBias = nnTwo.getNetworkBias();
		double[] kidsBias = new double[fathersBias.length];
		
		for(int i = 0; i < kidsBias.length; i++) {
			kidsBias[i] = (fathersBias[i] + momsBias[i]) / 2;
		}
		kidsBias = mutate(kidsBias);
		NeuralNet kid = new NeuralNet(template, nextGeneration.size()+1);
		kid.setBiases(kidsBias);
		nextGeneration.add(kid);
	}
	
	public double getBestInGen() {
		double bestboy = -1;
		for(NeuralNet nn : population) {
			if(bestboy < 0) {
				bestboy = nn.getErr();
			}
			else if(nn.getErr() < bestboy) {
				bestboy = nn.getErr();
			}
		}
		return bestboy;
	}
	
	public void genAlg() {
		fitness();
		generation = 1;
		do {
			if((generation % checkInterval) == 0)
				System.out.println("[Train error @" + generation + "]: " + getBestInGen());
			nextGeneration.clear();
			elitism();
			while(nextGeneration.size() < popsize)
				crossBreed(select(), select());
			population.clear();
			population.addAll(nextGeneration);
			fitness();
		} while (++generation < nofIter);
	}
	
	public void reparseInput(LinkedList<String> newInput) {
		for(NeuralNet nn : population) {
			nn.parseInput(newInput);
		}
	}
	
	public void test(LinkedList<String> newInput) {
		reparseInput(newInput);
		fitness();
		System.out.println("[Test error]: " + getBestInGen());
	}
}
